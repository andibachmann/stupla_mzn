
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "stupla_mzn/version"

Gem::Specification.new do |spec|
  spec.name          = "stupla_mzn"
  spec.version       = StuplaMzn::VERSION
  spec.authors       = ["Andi Bachmann"]
  spec.email         = ["bachmann@sonoqua.ch"]

  spec.summary       = %q{Convert a given .yaml file into a .dzn file.}
  spec.description   = %q{Convert a given .yaml file into a project specific .dzn file.}

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "wirble"

  spec.add_dependency "thor"
  # tod - Time of Day gem
  spec.add_dependency "tod"
end
