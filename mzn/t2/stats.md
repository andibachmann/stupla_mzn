
# model-2.mzn with t2.dzn

``` mzn
Compiling model-2.mzn, with additional data t2.dzn
Running model-2.mzn
start_slot = [| "0", "0", "1" |
   "0", "7", "0" |
   "13", "0", "0" |
   "10", "0", "0" |
   "7", "0", "0" |
   "0", "0", "10" |
   "0", "0", "7" |
   "0", "0", "4" |
   "1", "0", "0" |
   "0", "0", "13" |
   "0", "0", "33" |]
   
obj_stud   = 62 ;
obj_teacher= 84 ;
----------
Stopped.
Finished in 15m 23s
```

# model-no-sched.mzn with t2.dzn

```
Compiling model-no-sched.mzn, with additional data t2.dzn
Running model-no-sched.mzn
start_slot = [| "0", "0", "1" |
   "11", "0", "0" |
   "0", "0", "1" |
   "7", "0", "0" |
   "7", "0", "0" |
   "0", "0", "8" |
   "0", "0", "6" |
   "0", "0", "1" |
   "1", "0", "0" |
   "0", "0", "1" |
   "0", "0", "33" |]
obj_stud   = 60 ;
obj_teacher= 85 ;
----------
Stopped.
Finished in 25m 12s
```
