% enum of presence days
enum DAY;
% maximal duration of a lessons
int: maxDur;
% maximal numbers of slots per Day;
int: maxSlots;
set of int: SLOT  = 1..maxSlots;
set of int: SLOTx = 0..maxSlots;
% number of students
int: n;
set of int: STUDENT = 1..n;
%
array[DAY]         of set of SLOT: teacher;
array[STUDENT,DAY] of set of SLOT: feasible;
array[STUDENT]       of 1..maxDur: lessonDuration;
array[STUDENT,DAY,SLOT]   of 0..3: prio;
array[DAY,SLOT]           of 0..3: prioTeacher;
% Factor for weighting: obj = obj_stud + k * obj_teacher
int: k;
%
% decision VARIABLES
% array[STUDENT,DAY] of var 0..maxSlots: start_slot;
array[STUDENT,DAY] of var SLOTx: start_slot;
array[STUDENT,DAY] of var SLOTx: end_slot;

% 2d-array that stores for each d (in DAYS) and each SLOT 
%    the STUDENT or 
%    0  if it is not allocated or
%    -1 the teacher is available neither
array[SLOT,DAY] of var -1..n: schedule;

% -----------------------------------------------------------
% CONSTRAINTS 
constraint forall(s in STUDENT, d in DAY where start_slot[s,d] > 0)(
  start_slot[s,d] in feasible[s,d] );

constraint forall(s in STUDENT, d in DAY)(
  if start_slot[s,d] > 0 then
    end_slot[s,d] = start_slot[s,d] + lessonDuration[s] - 1
  else
    end_slot[s,d] = 0
  endif);
 
constraint forall(s in STUDENT, d in DAY where start_slot[s,d] > 0)( 
  forall(j in 1..lessonDuration[s]-1) ( start_slot[s,d] + j in feasible[s,d] )
  ); 

% make sure each student has exactly 1 lesson
constraint forall(s in STUDENT)( sum([start_slot[s,d] > 0| d in DAY]) = 1);

constraint forall(s in STUDENT, d in DAY, z in SLOT) (
  (z in feasible[s,d] /\ z >= start_slot[s,d] /\ z <= end_slot[s,d]) 
     <-> schedule[z,d] = s
  );

constraint forall(d in DAY, z in SLOT)(
  (z in teacher[d] /\ schedule[z,d] = -1) -> schedule[z,d] = 0 );

var int: obj_stud;
constraint obj_stud = sum([prio[schedule[z,d],d,z]|
  d in DAY, z in SLOT where schedule[z,d] > 0]);

var int: obj_teacher;
constraint obj_teacher = sum([prioTeacher[d,z]|
  d in DAY, z in SLOT where schedule[z,d] > 0]);

%solve satisfy;
solve :: int_search( [start_slot[s,d] |s in STUDENT, d in DAY], 
  first_fail, indomain, complete) maximize (obj_stud + k * obj_teacher);

output [ 
  "start_slot = " ++ show2d(start_slot) ++ "\n" ++
  "end_slot   = " ++ show2d(end_slot) ++ "\n" ++
  " teacher   = " ++ show(teacher) ++ ";\n" ++
  " feasible  = " ++ show2d(feasible) ++ "\n" ++
  "schedule   = \n" ++ show2d(schedule) ++ ";\n"  ++
%  " - "
  "obj_stud   = \(obj_stud) ;\n" ++
  "obj_teacher= \(obj_teacher) ;\n"
];
