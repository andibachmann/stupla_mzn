#!/usr/bin/env ruby
#
garr3 = [0, 1, 0, 0, 1, 1, -1,-1, 0, 0 ,1, 1, 0, 1, 1, 0, 1,0,1,0, 0, -1, 1,0,1,1,-1,1,0,1]

DISP = [".","p","L"]

def gap_array(schedule_arr)
  # initialize variables
  start_search = false              # flag for break start
  break_slots  = 0                  # counter of break slots
  res_arr      = []                 # resulting array
  schedule_arr.each do |slot|
    if slot == 1                    # start watching for break
      start_search = true
    end
    #
    if start_search                 
      if    slot == 0               # == break
        break_slots += 1
      elsif slot == 1               # == consecutive lesson slot
        if break_slots > 0          # number of break_slots > 0
          res_arr.append(break_slots)
          break_slots = 0
        end
      else                          # == not available
        break_slots  = 0            # any break so far is discarded
        start_search = false         
      end
    end
  end
  return res_arr
end

# search for sequences like [..,1,0,0,-1] and convert the 0 into -1
#                           [..,-1,0,0,1] 
def eliminate_false_holes(gap_arr)
  na = false
  gap_arr.reduce([]) do |res,e|
    na = true if e == -1
    #
    if na == true
      if e <= 0
        res.push(-1)
      else
        na = false
        res.push(e)
      end
    else
      res.push(e)
    end
    res
  end
end
def show(s_arr)
  pre_arr = s_arr.reduce([]) { |res,e| v = e >= 0 ? 0 : -1; res.append(v); res }

  puts "  Schedule  : #{pp_arr(s_arr)}"
  puts "  Presence  : #{pp_arr(pre_arr)}"
  dup_arr = s_arr.dup
  
  dup_arr[0]  = -1 if s_arr.first == 0
  dup_arr[-1] = -1 if s_arr.last == 0
  fin = eliminate_false_holes(eliminate_false_holes(dup_arr).reverse).reverse
  
  puts "  Real Gaps : #{pp_arr fin}"
  c3_r = gap_array(dup_arr)
  puts "    => Gaps : #{c3_r};\tCount: #{c3_r.size()}, Sum: #{c3_r.sum()}"
  #
end

def equal_and(z, y)
  if y
end
# ----------------------------- new part
# first slot  -> teacher free = no lesson in the slot
# other slots -> teacher free = teacher out or (left slot teacher free and no lesson in slot)
def teacher_left(lesson)
  tftmp = []
  tftmp[0] = (1-lesson[1])
  #
  n = lesson.size
  (1..n).each do |i|
    tfl[i-1] = equal_and(tftmp[i], 1-lesson[i])
    puts [tftmp[i], tfl[i-1], 1-lesson[i]]
  end

end

def new_algo(schedule)
  puts "  Schedule  : #{pp_arr(schedule)}"
  # convert schedule to lesson: -1 -> 0
  lesson = schedule.map { |e| e < 0 ? 0 : e }
  puts "  lesson    : #{pp_arr(lesson)}"
  tfl = teacher_left(lesson)
  puts "  tfl       : #{pp_arr(tfl)}"
end

# utility
def pp_arr(sarr)
  darr = sarr.map { |e| DISP[e+1] }
  darr.join(" ")
end

# Examples:

puts "\nExample 3"
show(garr3)

puts "\n new Algo"
puts
puts new_algo(garr3)
