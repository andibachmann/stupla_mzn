%% gap algo
%% usage:
%%     minizinc -a --solver gecode gap.mzn 
%%
%% orig:       [p|S|p|p|S|S|·|·|p|p|·|S|p|p|p|S|p|S|S|·|p|S|p|]
%% holes:      [·|S|p|p|S|S|·|·|·|·|·|S|p|p|p|S|p|S|S|·|·|S|·|]
array[int] of int: hole_arr = [0,1,0,0,1,1,-1,-1, 0, 0 ,1, 1, 0, 1, 1, 0,1 ,0,1,0,0,-1,1,0,1,1,-1,1,0,1];
var set of SLOT: teacher;
int: maxSlots = length(hole_arr);
teacher = {1,2,3,4,5,6, 9,10,11,12,13,14,15,16,17,18,19,20,21,23,24,25,26,28,29,30};

set of int: SLOT = 1..maxSlots;
 %           -1   0   1      #=>    -1 := not available, 0 := free, := lesson
enum E = { '·', p, L };
enum F = { '_', 'T' };
          
array[SLOT] of var 0..1: lesson = [ bool2int(hole_arr[z] > 0) | z in 1..maxSlots];
array[SLOT] of var 0..1: teacher_free_left;
array[SLOT] of var 0..1: teacher_free_right;
array[SLOT] of var 0..1: teacher_free;
array[SLOT] of var 0..1: real_gap;
           
% ------------------------------------------------------
% predicates
predicate equals_and(var 0..1: z, var 0..1: x, var 0..1: y) = 
    (z <= x /\ z <= y /\ z >= x + y - 1);

predicate equals_or(var 0..1: z, var 0..1: x, var 0..1: y) = 
    (z >= x /\ z >= y /\ z <= x + y);
% calculate teacher free left
%    first slot  -> teacher free = no lesson in the slot
%    other slots -> teacher free = teacher out or (left slot teacher free and no lesson in slot)
array[SLOT] of var 0..1: teacher_free_left_temp;
constraint teacher_free_left_temp[1]=1-lesson[1];

constraint forall(z in 2..maxSlots)
    (equals_and(teacher_free_left_temp[z], teacher_free_left[z-1], 1-lesson[z]));

constraint forall(z in SLOT)
    (equals_or(teacher_free_left[z], 1 - bool2int(z in teacher), teacher_free_left_temp[z]));
 
    % calculate teacher free right
%    last slot   -> teacher free = no lesson in the slot
%    other slots -> teacher free = teacher out or (right slot teacher free and no lesson in slot)
array[SLOT] of var 0..1: teacher_free_right_temp;
constraint teacher_free_right_temp[maxSlots]=1-lesson[maxSlots];
    
constraint forall(z in 1..maxSlots-1)
    (equals_and(teacher_free_right_temp[z], teacher_free_right[z+1], 1-lesson[z]));

constraint forall(z in SLOT)
    (equals_or(teacher_free_right[z], 1 - bool2int(z in teacher), teacher_free_right_temp[z]));

% teacher free when teacher free left or teacher free right
constraint forall(z in SLOT)
    (equals_or(teacher_free[z], teacher_free_left[z], teacher_free_right[z]));

% real gap when teacher not free and no lesson
constraint forall(z in SLOT)
   (equals_and(real_gap[z], 1-teacher_free[z], 1-lesson[z]));
        
solve satisfy;

output 
  [ "orig hole_arr      : ["] ++ [ show(E[z+2]) ++ "|" | z in hole_arr ] ++ [ "]\n" ] ++
  [ "teacher            : [" ] ++ [ show(E[1 + (z in teacher)]) ++ "|" | z in 1..maxSlots ] ++ [ "]\n\n" ] ++
  [ "lesson             : ["] ++ [ show(E[z+2]) ++ "|" | z in lesson ] ++ [ "]\n" ] ++
  [ "teacher_free_left  : ["] ++ [ show(F[z+1]) ++ "|" | z in teacher_free_left ] ++ [ "]\n" ] ++
  [ "teacher_free_right : ["] ++ [ show(F[z+1]) ++ "|" | z in teacher_free_right ] ++ [ "]\n" ] ++
  [ "teacher_free       : ["] ++ [ show(F[z+1]) ++ "|" | z in teacher_free ] ++ [ "]\n" ] ++
  [ "real_gap           : ["] ++ [ show(E[2-z]) ++ "|" | z in real_gap ] ++ [ "]\n\n" ] ++

    [ "lesson             : ["] ++ [ show_int(1,z) ++ "|" | z in lesson ] ++ [ "]\n" ] ++  
  [ "teacher_free_left  : ["] ++ [ show_int(1,z) ++ "|" | z in teacher_free_left ] ++ [ "]\n" ] ++  
  [ "teacher_free_right : ["] ++ [ show_int(1,z) ++ "|" | z in teacher_free_right ] ++ [ "]\n" ] ++  
  [ "teacher_free       : ["] ++ [ show_int(1,z) ++ "|" | z in teacher_free ] ++ [ "]\n" ] ++  
  [ "real_gap           : ["] ++ [ show_int(1,z) ++ "|" | z in real_gap ] ++ [ "]\n" ]  

;

