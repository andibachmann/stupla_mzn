Minizinc Objective Function For Gaps in Schedule
================================================

I have a working Miniznic model to schedule individual lessons of 1 professor
having n students (see ). The model considers availability (hard constraint)
and the time preferences (objective function) of the professor as well
as of the students.

Now, I want to extend the model and optimize the schedule such that
gaps between lessons are minimized.

Example:

```
   Schedule : p L p p L L . . p p L L p L L p L p p . L p L L . L p L
  Real Gaps : . L p p L L . . . . L L p L L p L . . . L p L L . L p L
```
where

  `p` = Teacher available, but no Lesson

  `L` = Teacher gives lesson (to student)

  `.` = Teacher not available
 
Obviously, the `p` in slot 1 must not be counted as a gap. Similarly,
slots 9 and 10 are no gaps, neither. Eliminating all false gaps the
above schedule becomes

Note: _false_ gaps are marked with `.` (the same as _not available_).

The result would be a gap array `[2, 1, 1, 1, 1]` from which one can
compute a penalty value by e.g. summing up.

In `ruby` I was able to do this, but I fail to formulate a function
for this. My algorithm in looks like this:

``` ruby
def gap_array(schedule_arr)
  # initialize variables
  start_search = false              # flag for break start
  break_slots  = 0                  # counter of break slots
  res_arr      = []                 # resulting array
  schedule_arr.each do |slot|
    if slot == 1                    # start watching for break
      start_search = true
    end
    #
    if start_search                 
      if    slot == 0               # == break
        break_slots += 1
      elsif slot == 1               # == consecutive lesson slot
        if break_slots > 0          # number of break_slots > 0
          res_arr.append(break_slots)
          break_slots = 0
        end
      else                          # == not available
        break_slots  = 0            # any break so far is discarded
        start_search = false         
      end
    end
  end
  return res_arr
end
```

Note: `arr` is the schedule array having `{".": -1, "p": 0, "L" : 1}`.

How can I formulate such a function in Minizinc?







 



