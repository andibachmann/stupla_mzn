% enum of presence days
enum DAY;
int: num_days = card(DAY);
% maximal duration of a lessons
int: maxDur;
% maximal numbers of slots per Day;
int: maxSlots;
set of int: SLOT  = 1..maxSlots;
set of int: SLOTx = 0..maxSlots;
% number of students
int: n;
set of int: STUDENT = 1..n;
%
array[DAY]         of set of SLOT: teacher;
array[STUDENT,DAY] of set of SLOT: feasible;
array[STUDENT]       of 1..maxDur: lessonDuration;
array[STUDENT,DAY,SLOT]   of 0..3: prio;
array[DAY,SLOT]           of 0..3: prioTeacher;
% Factor for weighting: obj = obj_stud + k * obj_teacher
int: k;

% Make the time axis one-dimensional and convert all data accordingly.
set of int: TIME = 1..(maxSlots*num_days);
% 
function int: time(int: d, int: z) = (d-1)*maxSlots + z;

set of TIME: teacher_time = {time(d, z) | d in DAY, z in teacher[d]};
array[STUDENT] of set of TIME: feasible_time = [{time(d, z) | d in DAY, z in feasible[s,d]} | s in STUDENT];
array[STUDENT] of set of TIME: feasible_start_time = 
    [{time(d,z) | d in DAY, z in 1..maxSlots-lessonDuration[s]+1 where 
        forall(u in time(d,z)..time(d,z)+lessonDuration[s]-1)
          (u in feasible_time[s] intersect teacher_time)
       } 
      | s in STUDENT];

array[STUDENT,TIME] of 0..3: prio_time = array2d(STUDENT, TIME, [prio[s,d,z] | 
  s in STUDENT, 
  d in DAY, 
  z in SLOT]);
array[TIME] of 0..3: prioTeacher_time = [prioTeacher[d,z] | d in DAY, z in SLOT]; 

%
% decision VARIABLES
array[STUDENT,TIME] of var 0..1: start;
array[STUDENT,TIME] of var 0..1: active;

% -----------------------------------------------------------
% CONSTRAINTS
% 1. a lesson can only start at a feasible time
constraint forall(s in STUDENT, t in TIME)(
  start[s,t] <= bool2int(t in feasible_start_time[s]));

% 2. each lesson must have a start time
constraint forall(s in STUDENT)
  (sum(t in TIME)(start[s,t]) = 1);

% 3. maximum one lesson active at any time         
constraint forall(t in TIME)
  (sum(s in STUDENT)(active[s,t]) <= 1);

% 4&5. constraints defining if lesson active
constraint forall(s in STUDENT, d in 1..num_days)
  (active[s,time(d,1)] = start[s,time(d,1)]);

constraint forall(s in STUDENT, d in 1..num_days, z in 2..maxSlots)  
  (active[s,time(d,z)] <= active[s,time(d,z-1)] + start[s,time(d,z)]);

% 6. ensure duration of lesson is fulfilled
constraint forall(s in STUDENT)
    (sum(t in TIME)(active[s,t]) = lessonDuration[s]);   

var int: obj = sum(s in STUDENT, t in TIME)
    (active[s,t] * (prio_time[s,t] + k*prioTeacher_time[t])); 
%%%
array[STUDENT] of var TIME: start_time;
include "disjunctive.mzn";
constraint disjunctive(start_time, lessonDuration);

constraint forall(s in STUDENT)
    (start_time[s] in feasible_start_time[s]);

solve maximize obj;

output [ "teacher_time        " ++ show(teacher_time) ++ "\n" ] ++
  [ "feasible_time       " ++ show(feasible_time) ++ "\n" ] ++
  [ "lessonDurations     " ++ show(lessonDuration) ++ "\n" ] ++
  [ "feasible_start_time " ++ show(feasible_start_time) ++ "\n" ] ++
  [ "start_time          " ++ show(start_time) ++ "\n" ] ++
  [ 
  "  Slot# ||"] ++
  [ "  \(d)  |" | d in DAY ] ++
 [
  "|  obj = " ++ show(obj) ++ 
  "   [teacher=\(sum(s in STUDENT, t in TIME)(active[s,t] * prioTeacher_time[t])), " ++
  "stud=\(sum(s in STUDENT, t in TIME)(active[s,t] * prio_time[s,t])), k=\(k)]" ] ++
[ "\n -------++"] ++ 
[ "-------+" | d in DAY ] ++ 
["+\n"] ++
[
  if d = 1 then show_int(5,z) ++ "   ||" else "" endif ++
  show_int(4,let {var int: student = sum(s in STUDENT)(s*active[s,time(d,z)])} in if student > 0 then student else bool2int(z in teacher[d]) - 1 endif) ++ "   |" ++
  if d = num_days then "|\n" else "" endif | z in SLOT, d in DAY
] ++ [ " -------++"] ++
  [ "-------+" | d in DAY ]  
  ++ ["+\n"] ++
  [ "start     \n" ++ show2d(start)  ++ "\n" ] ++
  [ "active    \n" ++ show2d(active) ++ "\n" ]
;