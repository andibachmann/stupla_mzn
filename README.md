stupla_mzn
==========

This little gem will allow you to transform a given timetable
configuration (provided as `yaml` file) into `.dzn` format.

Please note that this gem is highly dependent on the actual 
[MiniZinc-Model](https://www.minizinc.org).

