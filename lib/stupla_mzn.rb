require 'stupla_mzn/version'
require 'stupla_mzn/cli'
require 'stupla_mzn/person'
require 'stupla_mzn/student'
require 'stupla_mzn/teacher'
require 'stupla_mzn/availability'
require 'stupla_mzn/week_schedule'
require 'stupla_mzn/day'
require 'stupla_mzn/timetable'
require 'stupla_mzn/mzn_wrapper'
require 'stupla_mzn/mzn_2darray'

# StuplaMzn **St**unden**p**lan
# provide basic library that wraps a human language (yaml-base) description
# of the single lesson timetabling (Constraint Optimization Problem).
module StuplaMzn
  # Your code goes here...
end
