module StuplaMzn
  class Mzn2darray
    def self.parse(cont)
      lines    = cont.lines
      arrl     = []
      schedule = []
      until lines.empty?
        line = lines.shift
        next unless line =~ /schedule\s+=/
        arrl << line.chomp
        until line =~ /;/
          line = lines.shift
          arrl << line.strip
        end
      end

      until arrl.empty?
        l = arrl.shift
        next unless array_start?(l)

        until array_end?(l)
          r = l.scan(/\"(-?\d+)\",\s+\"(-?\d+)/).first
          schedule << r.map(&:to_i)
          l = arrl.shift
        end
        r = l.scan(/\"(-?\d+)\",\s+\"(-?\d+)/).first
        schedule << r.map(&:to_i)
        l = arrl.shift
      end
      schedule
    end

    def self.array_start?(line)
      line =~ /\[\|/
    end

    def self.array_end?(line)
      line =~ /\|\]/
    end
  end
end
