# tod - time of day 
require 'tod'

module StuplaMzn
  class WeekSchedule
    attr_accessor :slot_duration
    attr_reader   :days
         
    WEEKDAYS = %w(day0 day1 day2 day3 day4 day5 day6)
    #WEEKDAY_NAMES = [:mo, :di, :mi, :do, :fr, :sa, :so]
    WEEKDAY_NAMES = %w(Montag Dienstag Mittwoch Donnerstag Freitag Samstag Sonntag).freeze

    def initialize(slot_dur_min)
      @slot_duration = slot_dur_min
      @days = []
      WEEKDAY_NAMES.each_with_index do |name,i|
        @days << Day.new(name,i,self)
      end
    end

    def add_availability(av)
      day_by_name(av.weekday)&.add_availability(av)
      #days(av.weekday.downcase).add_availability(av)
    end

    def wd_short_names
      @wd_short_names ||= WEEKDAY_NAMES.map { |d| d[0..1].downcase }
    end
    
    def day_by_name(name)
      i = wd_short_names.index(name[0..1].downcase)
      i.nil? ? nil : @days[i]
    end
    
    # 
    def available_days
      days.delete_if { |d| d.start_of_day.nil? }
    end

    def available_days_index
      available_days.map { |d| d.day_of_week }
    end
    # (0..6).to_a.delete_if { |di| send("day#{di}").start_of_day.nil? }

    def available_day_names
      #WEEKDAY_NAMES.values_at(*available_days)
      available_days.map { |d| d.name }
    end

    def day(d)
      if d.kind_of?(Integer) && (0..6).include?(d)
        @days[d]
      elsif d.kind_of?(String)
        day_by_name(d)
      end
    end

    def earliest_start_slot
      @earliest_start_slot ||= @days.map { |a| a.slots.min }.compact.min
    end

    def earliest_start_time
      @earliest_start_time ||= @days.map { |a| a.start_of_day }.compact.min
    end

    def latest_end_slot
      @latest_end_slot ||= @days.map { |a| a.slots.max }.compact.max
    end

    def latest_end_time
      @latest_end_time ||= @days.map { |a| a.end_of_day }.compact.max
    end

    def slot_span
      latest_end_slot - earliest_start_slot + 1
    end
  end
end
