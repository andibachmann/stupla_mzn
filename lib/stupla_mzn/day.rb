require 'set'

module StuplaMzn
  class Day
    attr_reader   :start_of_day
    attr_accessor :end_of_day
    attr_accessor :slots
    attr_reader   :day_of_week
    attr_reader   :name

    def initialize(name, dow_i, week_schedule)
      @day_of_week   = dow_i
      @name          = name
      @week_schedule = week_schedule
      @slots         = SortedSet.new
      @start_of_day  = nil
      @end_of_day    = nil
    end

    # 15 min ->   96 slots / day
    # 10 min ->  144 slots / day
    #  5 min ->  288 slots / day
    #  1 min -> 1440 slots / day
    def slot_duration
      @week_schedule.slot_duration
    end

    def slot_dur_sec
      slot_duration * 60
    end

    def set_start_of_day(stod)
      @start_of_day = stod
    end

    def add_availability(av)
      st = av.start_time
      et = av.end_time

      if start_of_day.nil? 
        @start_of_day = st
        @end_of_day = et
        @slots = av.day_slot_set(slot_duration)
      else
        if st < start_of_day
          set_start_of_day(st)
        end
        if et > end_of_day
          @end_of_day = et
        end
        @slots = @slots.union(av.day_slot_set(slot_duration))
      end
    end

    def show
      puts "#{start_of_day} -- #{end_of_day} [#{slot_duration}]: #{slots}"
    end
  end
end
