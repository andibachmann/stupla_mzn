require 'thor'

module StuplaMzn
  # command line options and argument
  class Cli < Thor
    desc 'convert_to_dzn YAML_FILE', 'use YAML_FILE to calculate!'
    option :outfile, aliases: "-o", desc: "output file"
    def convert_to_dzn(yaml_file)
      timetable = Timetable.parse_file(yaml_file)
      timetable.write_dzn_file(options[:outfile])
    end

    desc 'calc DATA_FILE', 'use DATA_FILE -> [array of sets]'
    def calc(data_file)
      timetable = Timetable.parse_file(data_file)
      timetable.calc
    end
  end
end
