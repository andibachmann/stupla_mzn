# tod - time of day 
require 'tod'

module StuplaMzn
  # # The timetable
  # The timetable class provide is the central object of every timetabling
  # problem.
  #
  class Timetable
    attr_accessor :slot_duration
    attr_accessor :data
    attr_reader   :teacher
    attr_reader   :students
    attr_accessor :data_file
    WEEKDAYS      = %w[Mo Di Mi Do Fr Sa So)].freeze

    def initialize
      @students = []
    end

    def slot_dur_sec
      @slot_dur_sec ||= slot_duration * 60
    end

    def self.parse_file(data_file)
      timetable           = Timetable.new
      timetable.data_file = data_file
      timetable.parse_data
      timetable
    end

    def parse_data
      data = YAML.load_file(data_file)
      if data && !data.empty?
        # 1. timetable
        @slot_duration = data[:timetable][:slot_duration]
        # 2. teacher
        add_teacher(Teacher.new(data[:teacher][:name]))
        data[:teacher][:availabilities].each do |a|
          @teacher.add_availability(StuplaMzn::Availability.new(*a))
        end
        # 3. students
        data[:students].each do |stud|
          st = Student.new(stud[:name])
          st.duration = stud[:duration]
          add_student(st)
          stud[:availabilities].each do |a|
            st.add_availability(StuplaMzn::Availability.new(*a))
          end
        end
      end
    end

    def add_teacher(teacher)
      @teacher = teacher
      @teacher.timetable = self
    end

    def add_student(student)
      @students << student
      student.timetable = self
    end

    def master_schedule
      teacher.schedule
    end

    def print_teacher_avail
      puts teacher_avail_str
    end

    def teacher_avail_str
      ads_i  = master_schedule.available_days_index
      ms = ads_i.map do |di|
        '{' + teacher.slots_based_to_dzn(di).to_a.join(',') + '}'
      end.join(",\n  ")
      msg = 'teacher = [ '
      msg = msg + ms + '];'
      msg
    end
    
    def teacher_prio_array_str
      msg = []
      msg << '% Prioririties of Teacher'
      msg << 'prioTeacher = array2d(DAY,1..maxSlots, ['
      prio_arr = teacher.priorities_array
      msg << prio_arr.map { |d| '  ' + d.join(',') }.join(",\n") + ']);'
      msg.join("\n")
    end

    def students_prio_array_str
      msg = []
      msg << '% Priorities of Students'
      msg << 'prio  = array3d(1..n,DAY,1..maxSlots, ['
      nr = students.size - 1
      students.each_with_index do |stud,i|
        msg << '  %% %2d. %s' % [i+1, stud.name ]
        ps = stud.priorities_array
        tstr = ps.map { |d| '  ' + d.join(',') }.join(",\n")
        if i < nr
          msg << tstr + ','
        else
          msg << tstr + ']);'
        end
      end
      msg.join("\n")
    end

    def lesson_durations_str
      dur_arr = students.map { |s| s.duration / slot_duration }.join(',')
      "lessonDuration = [#{dur_arr}];"
    end

    def to_dzn_str
      cnt = []
      ad_names = available_day_names.join(', ')
      max_dur = students.map { |s| s.duration / slot_duration }.max   
      ads_i  = master_schedule.available_days_index
      #
      feasi = students.map do |stud|
        "  % #{stud.name}\n  " +
        + ads_i.map do |di|
          '{' + stud.slots_based_to_dzn(di).to_a.join(',') + '}'
        end.join(", ")
      end
      # 
      cnt << "% Datafile"
      cnt << '% Available week days'
      cnt << "DAY      = {#{ad_names}};"
      cnt << '% Number of maximal slots per day'
      cnt << "maxSlots = #{max_slots_per_day};"
      cnt << '% Number of students'
      cnt << "n        = #{students.size};"
      cnt << '% Weighting factor'
      cnt << "k        = 1;"
      cnt << lesson_durations_str
      cnt << "maxDur   = #{max_dur};"
      cnt << teacher_avail_str
      cnt << '% feasible time slots (teacher and students intersected)'
      cnt << 'feasible = array2d(1..n, DAY, ['
      cnt << feasi.join(",\n")
      cnt << ']);'
      cnt << teacher_prio_array_str
      cnt << students_prio_array_str
      cnt.join("\n")
    end

    def available_days
      master_schedule.available_days
    end

    def available_day_names
      master_schedule.available_day_names.map { |d| d.to_s.capitalize }
    end

    def max_slots_per_day
      master_schedule.slot_span
    end

    def write_dzn_file(out_name = nil)
      @dzn_file = out_name || File.basename(data_file).gsub(/.yaml/,'.dzn')
      File.write(@dzn_file, to_dzn_str)
    end
    
    # calc the schedule!
    def calc
      outfile   = File.basename(data_file).gsub(/.yaml/,'.dzn')
      File.write(outfile, to_dzn_str)
      mznw = StuplaMzn::MznWrapper.new('mzn/stupla-prio.mzn')
      # sch_arr = 2d-Array [slots,days]
      sch_arr = mznw.run(outfile)
      stt     = master_schedule.earliest_start_time
      adays   = master_schedule.available_days.map { |d| d.name[0..1] }
      i       = 0
      puts format("     %6s  |   %2s    |    %2s   |", 'Slot' , *adays)
      puts "  -----------+---------+---------+"
      sch_arr.each do |slot|
        st = stt + i*slot_duration*60
        puts format("  %2s  %6s |   %2s    |    %2s   |", i += 1,st, *slot)
      end

    end
  end
end
