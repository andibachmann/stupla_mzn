# tod - time of day 
require 'tod'
require 'forwardable'
require 'set'

module Tod
  class TimeOfDay
    def to_s
      strftime("%H:%M")
    end
  end
end

module StuplaMzn
  class Availability
    include Comparable

    # period = Tod::Shift(<tod::TimeOfDay>, <tod::TimeOfDay>)
    attr_accessor :period
    attr_accessor :weekday
    attr_accessor :priority
    
    extend Forwardable
    def_delegators :@period, :duration, :beginning, :ending
    def_delegator :@period, :beginning, :start_time
    def_delegator :@period, :ending, :end_time    
    
    WDS = %w(Mo Di Mi Do Fr Sa So)
    #attr_accessor :weekday, :start_time, :end_time

    def initialize(weekday, start_time, end_time, priority=0)
      @priority   = priority
      @weekday    = validate_weekday(weekday)

      st_time = Tod::TimeOfDay.parse(start_time)
      en_time = Tod::TimeOfDay.parse(end_time)
      if st_time > en_time
        puts "st_time = '#{st_time}'"
        puts "en_time = '#{en_time}'"        
        raise "start time is after end time!"
      end

      @period = Tod::Shift.new(st_time, en_time, exclude_end = true)
    end

    def validate_weekday(wd_str)
      wd_t = wd_str[0..1].capitalize
      WDS.include?(wd_t) && wd_t
    end

    def <=>(other)
      [WDS.index(@weekday), start_time] <=> [WDS.index(other.weekday), other.start_time]
    end
    
    def to_arr
      [@weekday, start_time, end_time]
    end
    
    def to_s
      "<#{self.class.name}:#{object_id}: #{@weekday}:#{start_time} - #{end_time}>"
    end

    def day_of_week
      WDS.index(@weekday)
    end
    
    def day_slot_set(slot_duration)
      step_sec = 60*slot_duration
      SortedSet.new(period.range.step( step_sec ).map { |s|
                      s / step_sec + 1})
    end
  end
end
