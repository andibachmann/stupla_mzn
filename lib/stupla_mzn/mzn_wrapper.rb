module StuplaMzn
  class MznWrapper
    CMD = "mzn-gecode"
    OPTS = "-o mzn-output.txt"
    attr_reader :mzn_model
    def initialize(mzn_model)
      @mzn_model = mzn_model
    end
    
    def run(dzn_file = '')
      cmd = "#{CMD} #{OPTS} #{mzn_model} #{dzn_file}"
      puts "Issuing system call '#{cmd}'"
      res = `#{cmd}`
      unless $?.exitstatus
        warn "oops!"
        warn "message = '#{res}'"
        exit
      end
      cont = File.read('mzn-output.txt')
      schedule = StuplaMzn::Mzn2darray.parse(cont)
      schedule
      # i = 0
      # puts format("    %s  |   %2s    |    %2s   |", 'Slot' , "Mo", "Di")
      # puts "  --------+---------+---------+"
      # schedule.each do |slot|
      #   puts format("     %2s   |   %2s    |    %2s   |", i += 1 , *slot)
      # end
    end
  end
end
