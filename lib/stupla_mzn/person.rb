require'yaml'

module StuplaMzn
  # Each person has a schedule (personal availabilities)
  class Person
    attr_accessor :name
    attr_accessor :availabilities
    attr_accessor :timetable

    def initialize(name)
      @name           = name
      @availabilities = []
    end

    def master_schedule
      @timetable.master_schedule
    end
    
    def schedule
      @schedule ||= WeekSchedule.new(timetable.slot_duration)
    end

    def add_availability(availability)
      @availabilities << availability
      schedule.add_availability(availability)
    end

    def slots_to_dzn(day)
      schedule.day(day).slots
    end

    def slots_based_to_dzn(day)
      master_slots = timetable.master_schedule.day(day).slots
      (master_slots & schedule.day(day).slots)
        .map { |e| e - (master_slots.first - 1) }.to_set
    end

    def priorities_array
      slot_min = master_schedule.earliest_start_slot
      slot_max = master_schedule.latest_end_slot
      z_size   = timetable.max_slots_per_day
      d_size   = master_schedule.available_days.size

      prio = Array.new(d_size) { Array.new(z_size, 0 ) }
      # puts "prio[#{d_size}][#{z_size}]"
      # puts "prio.first.size = #{prio.first.size}'"
      @availabilities.each do |avail|
        avail.day_slot_set(master_schedule.slot_duration).each do |z|
          prio[avail.day_of_week][z - slot_min] = avail.priority
        end
      end
      prio
    end
  end
end
