RSpec.describe Stp::Person do
  describe "Creating a new person" do
    it "#new('Fritz')" do
      person = Stp::Person.new("Fritz")
      expect(person.name).to eq('Fritz')
    end


    describe "#get_availabilities" do
      before(:context) do
        @person = Stp::Person.new("Fritz")
        timetable = Stp::Timetable.new()
        timetable.slot_duration = 15
        @person.timetable = timetable
        # @person.add_schedule(15)
        @av1 = Stp::Availability.new("Mo", "10:45", "12:00")
        @av2 = Stp::Availability.new("Di", "10:00", "10:15")
        @person.add_availability(@av1)
        @person.add_availability(@av2)
      end
      
      it "#get_availabilities" do
        avs = @person.availabilities
        expect(avs).to be_kind_of(Array)
        expect(avs.size).to eq(2)
      end

      it '#avalabilities on "Mo"' do
        avsm = @person.availabilities.select { |a| a.weekday == "Mo" }
        warn avsm
        expect(avsm).to be_kind_of(Array)
        expect(avsm.size).to eq(1)
        expect(avsm.first).to eq(@av1)
      end

      it "#slots_of_day('Mo') => [52] " do
        av3 = Stp::Availability.new("Mo", "12:45", "13:00")
        p   = Stp::Person.new('fritz')
        t   = Stp::Timetable.new()
        t.slot_duration = 15
        t.add_teacher(p)
        p.add_availability(av3)
        expect(p.slots_to_dzn('mo').to_a).to eq([52])
      end
    end

  end
end
