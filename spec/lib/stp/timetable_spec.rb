RSpec.describe Stp::Timetable do
  before(:context) do
    @timetable = Stp::Timetable.parse_file("test/data/test1.yaml")
  end
  
  it "#slots == 6" do
    expect(@timetable.slot_duration).to eq(15)
  end

  it "#teacher.name = 'Prof. Belcanto'" do
    expect(@timetable.teacher.name).to eq("Prof. Belcanto")
  end

  it "#available_days" do
    expect(@timetable.available_day_names).to eq(['Mo', 'Di'])
  end
end
    
