RSpec.describe Stp::WeekSchedule do
  it "#add_availability" do
    av1 = Stp::Availability.new("Mo", "10:00", "12:00")
    av2 = Stp::Availability.new("Mo", "13:00", "14:00")
    sch = Stp::WeekSchedule.new(15)
    sch.add_availability(av1)
    expect(sch.day0.start_of_day.to_s).to eq("10:00")
    sch.add_availability(av2)
    expect(sch.day0.end_of_day.to_s).to eq("14:00")
  end
end
