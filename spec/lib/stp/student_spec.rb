RSpec.describe Stp::Student do
  describe "Creating a new student" do
    
    it "#new('Fritz')" do
      stud = Stp::Student.new("Fritz")
      expect(stud.name).to eq('Fritz')
    end

    describe "Availabilities" do
      before(:context) do
        @student = Stp::Student.new("Fritz")
      end

      it "#availabilities is empty" do
        expect(@student.availabilities).to be_empty
      end

      it "#availabitlities << [<avail...>]" do
        @student.availabilities << Stp::Availability.new("Mo", "10:45", "12:15")
        expect(@student.availabilities).not_to be_empty
        #warn @student.availabilities.inspect
      end

      it "#availabitlities << [<avail...>]" do
        @student.availabilities << Stp::Availability.new("Mo", "16:45", "18:00")
        expect(@student.availabilities).not_to be_empty
        #warn @student.availabilities.inspect
        expect(@student.availabilities.size).to eq(2)
      end
    end
  end
end
