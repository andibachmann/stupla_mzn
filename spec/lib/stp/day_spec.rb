RSpec.describe Stp::WeekSchedule do
  it "#slot_duration" do
    sch = Stp::WeekSchedule.new(15)
    warn sch.day(1).slot_duration
    av1 = Stp::Availability.new("Mo", "10:00", "11:00")
    av2 = Stp::Availability.new("Mo", "09:00", "09:30")
    sch.add_availability(av1)
    warn sch.day1.show
    sch.add_availability(av2)
    warn sch.day1.show
  end

end
