RSpec.describe Stp::Availability do
  before(:example) do
    @availability = Stp::Availability.new('Mo', '10:40', '13:15')
  end
    
  it "#to_arr returns ARRAY" do
    stp = @availability.to_arr
    expect(stp).to be_kind_of(Array)
  end

  it "#weekday is 'Mo'" do
    expect(@availability.weekday).to eq('Mo')
  end

  it "#start_time is '10:40'" do
    expect(@availability.start_time.strftime("%H:%M")).to eq('10:40')
  end

  it "raises an error when start_time > end_time" do
    expect{ Stp::Availability.new('Mo', '15:40', '13:15') }.
      to raise_error(RuntimeError, /start time is after end time/)
  end

  it "sorts availabilities" do
    av1 = Stp::Availability.new('Di', '08:40', '10:00')
    av2 = Stp::Availability.new('Mo', '08:20', '19:00')
    av3 = Stp::Availability.new('Mo', '08:40', '10:00')
    expect( [av1, @availability].sort.reverse.first ).to eq(av1)
    expect( [av1, av2, av3].sort.first ).to eq(av2)
    expect( [av1, av3].sort.first ).to eq(av3)
  end

  it "00:00 - 00:30 (15 min. slot) -> slots: {1,2}" do
    av1 = Stp::Availability.new('Mo', '00:00', '00:30')
    expect(av1.day_slot_set(15)).to eq(Set[1,2])
  end

  it "23:30 - 23:45 (15 min. slot) -> slots: {95}" do
    av1 = Stp::Availability.new('Mo', '23:30', '23:45')
    expect(av1.day_slot_set(15)).to eq(Set[95])
  end

  
end
